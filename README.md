# ABOUT

Qute *(query + cute, pronounced like cute)* is a small JavaScript library that simplifies JavaScript while still being lightweight.

## Main features

-   write **clearer** code in **less time**
-   almost **no overhead**
-   **simplified management** of event listeners
-   works **perfectly together with vanilla code**
-   **easy to hack** - simply customize your version of Qute

## Qute vs. jQuery

Though Qute and jQuery have several features in common both libraries work fundamentally different. jQuery was created in a time when there where few standards across the web and compatibility with multiple browsers was a huge problem for web developers. Therefore jQuery uses its own unified interface to manipulate DOM nodes instead of the JavaScript API of the browser.  
In the past few years however, vanilla JavaScript and its standards have dramatically improved. Browsers are now widely compatible and many new features were implemented that previously needed some extra code or JS libraries like jQuery to work. Sadly jQuery can't profit from these innovations because many of those new vanilla features can't be accessed though the interface jQuery offers.  
On the other side Qute uses a completely different approach. Instead of creating its own interface, Qute just extends vanilla JavaScript with several valuable additions. This makes Qute fast, clear and easy to learn.

# Documentation

## Vanilla abbreviations

Some JavaScript objects and functions have very long names though they are used very frequently. Using these function names can be tedious over time and reduces readability especially in chained methods. Therefore Qute provides several abbreviations that are faster to write and easier to read.

`w` -> window  
`d` -> document  
`V` -> document.querySelector  
`Y` -> document.querySelectorAll  
`d.cElem` -> document.createElement  
`d.cText` -> document.createTextNode

```javascript
let a = V('#test');  // get element with id 'test'
```

## Qute functions

Qute objects are just DOM nodes that provide additional features. Using the following functions to receive Qute objects won't change any values of the objects but simply add some methods to them.

`qObj` -> converts a object to a Qute object by adding specific member variables and methods  
`Q` -> returns a Qute object similar to document.querySelector  
`A` -> returns Qute objects similar to document.querySelectorAll  

```javascript
let a = Q('#test');        // get Qute object with id 'test'
let b = A('.test');        // get Qute objects with class 'test'

let c = qObj(V('#test'));  // same as Q('#test')
```

## Event interface of Qute objects

Qute provides an event cache that stores event properties inside of objects so events can be easily removed later. This is especially useful when using anonymous functions in event listeners.

`node.ev` -> the array that stores information about cached events. Usually this shouldn't be changed except you explicitly need it.

`node.aEv` -> addEventListener  
`node.cEv` -> addEventListener + caching (to remove event listeners later)  
`node.rEv` -> removeEventListener + remove cached events. Following syntaxes are allowed:  

-   like removeEventListener, does nothing special
-   only the name of a cached event (e.g. 'click') as argument, removes specific event listeners  
-   no argument, removes all cached event listeners

```javascript
let a = Q('.test');        // get Qute object with class name 'test'
a.aEv('mouseover', func);  // add event listener to a

a.cEv('click', () => {     // add anonymous function as event listener
  /* code... */
});

a.rEv('click');  // remove cached event with name 'click'
```

## Queries and DOM interface of Qute objects

> Using queries on objects to search their children is more efficient than using the default query selector that starts at the document root.

`node.Q` -> node.querySelector  
`node.A` -> node.querySelectorAll  
`node.aChild` -> node.appendChild  
`node.rChild` -> node.removeChild

```javascript
let p = Q('div');      // get Qute object with tag name 'div'
let c = p.Q('#test');  // query for children of p with id 'test'

p.rChild(c);           // remove child c of p
```

# Benchmarks

The following results are based on values measured with the benchmark of the Qute snippets project (<https://gitlab.com/AaronErhardt/qute-snippets>).  
In each benchmark a single function was executed _10.000 times_, the time all iterations took was measured in _milliseconds_. Additionally each measurement was repeated five times and calculated into an average value to avoid irregularities.  

## Vanilla abbreviations

As expected vanilla abbreviations don't create any measurable overhead...

## Qute specific functions

| Firefox 67 (less is better)            | vanilla | Qute  |
| -------------------------------------- | ------- | ----- |
| Query selector (first run)             | 30 ms   | 74 ms |
| Query selector (further runs)          | 18 ms   | 18 ms |
| Add event listener (Qute used caching) | 27 ms   | 27 ms |

**Conclusion:**  
On Firefox Qute performed very well and only slowed down at the first query selector call. The reason for this is simple: The Q function calls the qObj function to convert HTML nodes to Qute objects. Once a node is a Qute object it will keep its Qute members and methods until it is deleted. So the next time qObj is called on the same node, it doesn't need to do anything because the node is already a Qute object. Therefore further queries have basically zero overhead.  
Surprisingly the cEv method that adds an event listener and also caches the properties of the listener performed just like the vanilla addEventListener function.

| Chromium 75 (less is better)              | vanilla | Qute  |
| -------------------------------------- | ------- | ----- |
| Query selector (first run)             | 44 ms   | 58 ms |
| Query selector (further runs)          | 42 ms   | 44 ms |
| Add event listener (Qute used caching) | 30 ms   | 43 ms |

**Conclusion:**  
On Chromium Qute had the same issue with the first query selector call as on firefox though it performed better on it. Further query selector calls had nearly no overhead, but on the other side the cEv function didn't perform as well.

# Demo

A small code example to find out how Qute works is provided in the src/demo folder. It can be opened at this link: <https://aaronerhardt.gitlab.io/qute/demo/>.

# Setup Qute

To use Qute in your projects you just need to add the following script tags to your html. Alternatively you use a copy of Qute on your own web server (which might be faster):

-   Regular: &lt;script src="<https://aaronerhardt.gitlab.io/qute/qute.js>">&lt;/script>
-   Minified: &lt;script src="<https://aaronerhardt.gitlab.io/qute/qute.min.js>">&lt;/script>
-   Module: Instructions for using Qute as an ES6 module are included in the qute-module.js file (<https://aaronerhardt.gitlab.io/qute/qute-module.js>).
