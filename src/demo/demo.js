w.cEv("load", () => { // add cached event

  w.rEv(); // removes all cached events


  // adding 10 new paragraphs

  let body = Q("body");

  for (let i = 10; i >= 0; --i) {
    let elem = d.cElem("p");
    elem.classList.add("test");

    let text = d.cText("(Added element) Click me!");
    elem.aChild(text);

    body.aChild(elem);
  }

  // add events

  let all = A(".test"); // get all elements with class name 'test' as Qute objects
  let test = Q("#test"); // get element with id 'test' as a Qute object

  test.style.fontWeight = "bold"; // like vanilly JS

  // the let i can be accessed through the anonymous function, so every element knows its number
  for (let i = all.length - 1; i >= 0; --i) {

    all[i].cEv("click", ev => { // add cached event

      all[i].rEv(); // remove cached event
      all[i].textContent = "clicked element #" + i + ", anonymous event listener was removed!";
    });
  }

  // set event listeners for the test node below the heading

  test.cEv("contextmenu", ev => {
    ev.preventDefault();
    test.textContent = `-${test.textContent}-`;
  });

  test.cEv("click", () => {
    test.rEv();
    test.textContent += " all events removed :)";
  });
});
