/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* Qute can be now imported using from a module script:
  import {
    V,
    Y,
    w,
    d,
    qObj,
    Q,
    A
  } from "path/to/file";
*/

// functions used in qObj

// ---> functions used in qObj

// EVENT MANAGMENT

function __aEv(ev, func, opt = false) {
  this.addEventListener(ev, func, opt);
}

function __cEv(name, func, opt = false) {
  this.ev.push({
    name: name,
    func: func,
    opt: opt
  });
  this.addEventListener(name, func, opt);
}

function __rEv(ev, func, opt = false) {

  if (ev === undefined) {
    for (let i = this.ev.length - 1; i >= 0; --i)
      this.removeEventListener(this.ev[i].name, this.ev[i].func, this.ev[i].opt);

    this.ev = [];

  } else if (func === undefined) {

    for (let i = this.ev.length - 1; i >= 0; --i)
      if (ev === this.ev[i].name) {
        this.removeEventListener(ev, this.ev[i].func, this.ev[i].opt);
        this.ev.splice(i, 1);
        return;
      }

  } else
    this.removeEventListener(ev, func, opt);
}

// QUERIES

function __Q(q) {
  return qObj(this.querySelector(q));
}

function __A(q) {
  let t = this.querySelectorAll(q);
  let c = [];

  for (let i = t.length - 1; i >= 0; --i) {
    c[i] = qObj(t[i]);
  }

  return c;
}

// DOM

function __aChild(c) {
  return this.appendChild(c)
}

function __rChild(c) {
  return this.removeChild(c)
}

// Error message
const __UNKNOWN_OBJ = "QUTE: Unknown object!";

// ---> qObj

const qObj = o => {

  if (o === null || o === undefined) {
    console.error(__UNKNOWN_OBJ, o);
    return o;
  }

  if (o.ev !== undefined)
    return o;

  // event managment

  o.ev = [];
  o.aEv = __aEv;
  o.rEv = __rEv;
  o.cEv = __cEv;

  // queries

  o.Q = __Q;
  o.A = __A;

  // DOM operations

  o.aChild = __aChild;
  o.rChild = __rChild;

  return o; // return qute object
};


// ---> further functions

// further qute objects

const w = qObj(window);

const d = qObj(document);

// vanilla abbreviations

d.cElem = e => qObj(d.createElement(e));

d.cText = t => d.createTextNode(t);

const V = q => d.querySelector(q);

const Y = q => d.querySelectorAll(q);

// queries

const Q = q => qObj(d.querySelector(q));

const A = q => {
  let t = d.querySelectorAll(q);
  let c = [];

  for (let i = t.length - 1; i >= 0; --i)
    c[i] = qObj(t[i]);

  return c;
};

export {
  V,
  Y,
  w,
  d,
  qObj,
  Q,
  A
};
